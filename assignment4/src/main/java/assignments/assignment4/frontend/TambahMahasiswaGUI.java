package assignments.assignment4.frontend;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI extends JFrame {

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Tambah Mahasiswa
        //set layout box
        Box box = Box.createVerticalBox();
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        box.add(Box.createVerticalGlue());

        //set label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);

        box.add(Box.createVerticalStrut(15));

        //set label Nama:
        JLabel labelNama = new JLabel();
        labelNama.setText("Nama:");
        labelNama.setForeground(SistemAkademikGUI.fontColor);
        labelNama.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNama);

        box.add(Box.createVerticalStrut(10));

        //set text field nama
        JTextField namaText = new JTextField();
        namaText.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaText.setMaximumSize(new Dimension(200, 140));
        box.add(namaText);

        box.add(Box.createVerticalStrut(15));

        //set label NPM:
        JLabel labelNPM = new JLabel();
        labelNPM.setText("NPM:");
        labelNPM.setForeground(SistemAkademikGUI.fontColor);
        labelNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNPM);

        box.add(Box.createVerticalStrut(15));

        //set text field nama
        JTextField NPMText = new JTextField();
        NPMText.setAlignmentX(Component.CENTER_ALIGNMENT);
        NPMText.setMaximumSize(new Dimension(200, 140));
        box.add(NPMText);

        box.add(Box.createVerticalStrut(15));

        //set button "Tambahkan"
        JButton btnTambahMahasiswa = new JButton("Tambahkan");
        btnTambahMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        btnTambahMahasiswa.setBackground(SistemAkademikGUI.orangeButton);
        btnTambahMahasiswa.setForeground(Color.white);
        btnTambahMahasiswa.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                //cek apakah ada isi dari text field
                if (namaText.getText().isEmpty() || NPMText.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh field");
                } else {
                    //cek apakah sudah ada objek mahasiswa dengan NPM yang sama
                    if (sudahAdaNPM(Long.valueOf(NPMText.getText()), daftarMahasiswa)) {
                        JOptionPane.showMessageDialog(null, String.format("NPM %s sudah pernah ditambahkan sebelumnya", NPMText.getText() ));
                    } else {
                        Mahasiswa mahasiswa = new Mahasiswa(namaText.getText(), Long.valueOf(NPMText.getText()));
                        daftarMahasiswa.add(mahasiswa);
                        JOptionPane.showMessageDialog(null, String.format("Mahasiswa %s-%s berhasil ditambahkan", NPMText.getText(), namaText.getText()));
                    }
                    //menghapus isi text field
                    namaText.setText("");
                    NPMText.setText("");

                }
            }
        });
        btnTambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnTambahMahasiswa);

        box.add(Box.createVerticalStrut(15));

        //set button kembali
        JButton btnKembali = new JButton("Kembali");
        btnKembali.setFont(SistemAkademikGUI.fontGeneral);
        btnKembali.setBackground(SistemAkademikGUI.blueButton);
        btnKembali.setForeground(Color.white);
        btnKembali.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });
        btnKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnKembali);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan TambahMahasiswaGUI ke frame SistemAkademik GUI
        frame.add(box);
    }

    /* method untuk cek apakah ada objek Mahasiswa terentu dalam sebuah array */
    public static boolean sudahAdaNPM(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        boolean sudahAda = false;
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm) {
                sudahAda = true;
            }
        }
        return sudahAda;
    }

}
