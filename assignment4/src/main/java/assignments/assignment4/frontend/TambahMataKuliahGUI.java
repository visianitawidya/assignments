package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI extends JFrame{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Tambah Mata Kuliah
        //buat layout box
        Box box = Box.createVerticalBox();
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        box.add(Box.createVerticalGlue());

        //set label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);

        box.add(Box.createVerticalStrut(15));

        //set label "Kode Mata Kuliah:"
        JLabel labelKode = new JLabel();
        labelKode.setText("Kode Mata Kuliah:");
        labelKode.setForeground(SistemAkademikGUI.fontColor);
        labelKode.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelKode.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelKode);

        box.add(Box.createVerticalStrut(12));

        //set text field kode
        JTextField kodeText = new JTextField();
        kodeText.setAlignmentX(Component.CENTER_ALIGNMENT);
        kodeText.setMaximumSize(new Dimension(200, 100));
        kodeText.setSize(90, 15);
        box.add(kodeText);

        box.add(Box.createVerticalStrut(15));

        //set label "Nama Mata Kuliah: "
        JLabel labelNama = new JLabel();
        labelNama.setText("Nama:");
        labelNama.setForeground(SistemAkademikGUI.fontColor);
        labelNama.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNama);

        box.add(Box.createVerticalStrut(12));

        //set text field nama
        JTextField namaText = new JTextField();
        namaText.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaText.setMaximumSize(new Dimension(200, 170));
        namaText.setSize(90, 15);
        box.add(namaText);

        box.add(Box.createVerticalStrut(15));

        //set label "SKS: "
        JLabel labelSKS = new JLabel();
        labelSKS.setText("SKS:");
        labelSKS.setForeground(SistemAkademikGUI.fontColor);
        labelSKS.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelSKS.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelSKS);

        box.add(Box.createVerticalStrut(12));

        //set text field SKS
        JTextField SKSText = new JTextField();
        SKSText.setAlignmentX(Component.CENTER_ALIGNMENT);
        SKSText.setMaximumSize(new Dimension(200, 170));
        box.add(SKSText);

        box.add(Box.createVerticalStrut(15));

        //set label "Kapasitas:"
        JLabel labelKapasitas = new JLabel();
        labelKapasitas.setText("Kapasitas:");
        labelKapasitas.setForeground(SistemAkademikGUI.fontColor);
        labelKapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelKapasitas.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelKapasitas);

        box.add(Box.createVerticalStrut(12));

        //set text field kapasitas
        JTextField kapasitasText = new JTextField();
        kapasitasText.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasText.setMaximumSize(new Dimension(200, 200));
        box.add(kapasitasText);

        box.add(Box.createVerticalStrut(15));

        //set button "Tambahkan"
        JButton btnTambahMatkul = new JButton("Tambahkan");
        btnTambahMatkul.setFont(SistemAkademikGUI.fontGeneral);
        btnTambahMatkul.setBackground(SistemAkademikGUI.orangeButton);
        btnTambahMatkul.setForeground(Color.white);
        btnTambahMatkul.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                //cek apakah ada isi dari text field
                if (namaText.getText().isEmpty() || kodeText.getText().isEmpty() || kapasitasText.getText().isEmpty() || SKSText.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh field");
                } else {
                    //cek apakah sudah ada objek mahasiswa dengan NPM yang sama
                    if (sudahAdaNama(namaText.getText(), daftarMataKuliah)) {
                        JOptionPane.showMessageDialog(null, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", namaText.getText() ));
                    } else {
                        MataKuliah mataKuliah = new MataKuliah(kodeText.getText(),namaText.getText(), Integer.valueOf(SKSText.getText()), Integer.valueOf(kapasitasText.getText()));
                        daftarMataKuliah.add(mataKuliah);
                        JOptionPane.showMessageDialog(null, String.format("Mata Kuliah %s berhasil ditambahkan", namaText.getText()));
                    }
                    //menghapus isi text field
                    kodeText.setText("");
                    namaText.setText("");
                    SKSText.setText("");
                    kapasitasText.setText("");
                }
            }
        });
        btnTambahMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnTambahMatkul);

        box.add(Box.createVerticalStrut(15));

        //set button kembali
        JButton btnKembali = new JButton("Kembali");
        btnKembali.setFont(SistemAkademikGUI.fontGeneral);
        btnKembali.setBackground(SistemAkademikGUI.blueButton);
        btnKembali.setForeground(Color.white);
        btnKembali.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });
        btnKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnKembali);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan TambahMataKuliahGUI ke frame SistemAkademik GUI
        frame.add(box);
    }

    /* method untuk cek apakah ada objek MataKuliah terentu dalam sebuah array */
    public static boolean sudahAdaNama(String nama, ArrayList<MataKuliah> arr) {
        boolean sudahAda = false;
        for (MataKuliah matkul : arr) {
            if (matkul.getNama().equals(nama)) {
                sudahAda = true;
            }
        }
        return sudahAda;
    }
}
