package assignments.assignment4.frontend;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mahasiswa
        //set layout box
        Box box = Box.createVerticalBox();
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        box.add(Box.createVerticalGlue());

        //set label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mahasiswa");
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);

        box.add(Box.createVerticalStrut(15));

        //set Label "Nama: "
        JLabel labelNama = new JLabel();
        labelNama.setText("Nama: " + mahasiswa.getNama());
        labelNama.setForeground(SistemAkademikGUI.fontColor);
        labelNama.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNama);

        box.add(Box.createVerticalStrut(15));

        //set Label "NPM: "
        JLabel labelNPM = new JLabel();
        labelNPM.setText("NPM: " + mahasiswa.getNpm());
        labelNPM.setForeground(SistemAkademikGUI.fontColor);
        labelNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNPM);

        box.add(Box.createVerticalStrut(15));

        //set Label "Jurusan: "
        JLabel labelJurusan = new JLabel();
        labelJurusan.setText("Jurusan: " + mahasiswa.getJurusan());
        labelJurusan.setForeground(SistemAkademikGUI.fontColor);
        labelJurusan.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelJurusan.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelJurusan);

        box.add(Box.createVerticalStrut(15));

        //set Label "Daftar Mata Kuliah:"
        JLabel labelDaftarMatkul = new JLabel();
        labelDaftarMatkul.setText("Daftar Mata Kuliah:");
        labelDaftarMatkul.setForeground(SistemAkademikGUI.fontColor);
        labelDaftarMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelDaftarMatkul.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelDaftarMatkul);

        box.add(Box.createVerticalStrut(15));

        //set daftar MataKuliah yang diambil Mahasiswa
        if (mahasiswa.getBanyakMatkul() > 0) {
            for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) {
                JLabel labelMatkul = new JLabel();
                labelMatkul.setText((i+1) + ". " + mahasiswa.getMataKuliah()[i].getNama());
                labelMatkul.setForeground(SistemAkademikGUI.fontColor);
                labelMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
                labelMatkul.setFont(SistemAkademikGUI.fontGeneral);
                box.add(labelMatkul);

                box.add(Box.createVerticalStrut(15));
            }
        } else {
            JLabel labelMatkul = new JLabel();
            labelMatkul.setText("Belum ada mata kuliah yang diambil");
            labelMatkul.setForeground(SistemAkademikGUI.fontColor);
            labelMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
            labelMatkul.setFont(SistemAkademikGUI.fontGeneral);
            box.add(labelMatkul);

            box.add(Box.createVerticalStrut(15));
        }

        //set Label "Total SKS: "
        JLabel labelTotalSKS = new JLabel();
        labelTotalSKS.setText("Total SKS: " + mahasiswa.getTotalSKS());
        labelTotalSKS.setForeground(SistemAkademikGUI.fontColor);
        labelTotalSKS.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelTotalSKS.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelTotalSKS);

        box.add(Box.createVerticalStrut(15));

        //set Label "Hasil Pengecekan IRS: "
        JLabel labelLabelCekIRS = new JLabel();
        labelLabelCekIRS.setText("Hasil Pengecekan IRS:");
        labelLabelCekIRS.setForeground(SistemAkademikGUI.fontColor);
        labelLabelCekIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelLabelCekIRS.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelLabelCekIRS);

        box.add(Box.createVerticalStrut(15));

        //set pengecekan IRS
        if (mahasiswa.getBanyakMasalahIRS() > 0) {
            for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) {
                JLabel labelMasalah = new JLabel();
                labelMasalah.setText((i+1) + ". " + mahasiswa.getMasalahIRS()[i]);
                labelMasalah.setForeground(SistemAkademikGUI.fontColor);
                labelMasalah.setAlignmentX(Component.CENTER_ALIGNMENT);
                labelMasalah.setFont(SistemAkademikGUI.fontGeneral);
                box.add(labelMasalah);

                box.add(Box.createVerticalStrut(15));
            }
        } else {
            JLabel labelMasalah = new JLabel();
            labelMasalah.setText("IRS Tidak Bermasalah");
            labelMasalah.setForeground(SistemAkademikGUI.fontColor);
            labelMasalah.setAlignmentX(Component.CENTER_ALIGNMENT);
            labelMasalah.setFont(SistemAkademikGUI.fontGeneral);
            box.add(labelMasalah);

            box.add(Box.createVerticalStrut(15));
        }

        //set button Selesai
        JButton btnSelesai = new JButton("Selesai");
        btnSelesai.setFont(SistemAkademikGUI.fontGeneral);
        btnSelesai.setBackground(SistemAkademikGUI.blueButton);
        btnSelesai.setForeground(SistemAkademikGUI.fontColor);
        btnSelesai.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });
        btnSelesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnSelesai);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan DetilRingkasanMahasiswaGUI ke frame SistemAkademik GUI
        frame.add(box);
    }
}
