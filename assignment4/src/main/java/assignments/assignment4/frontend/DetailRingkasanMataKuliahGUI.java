package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        //set layout box
        Box box = Box.createVerticalBox();
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        box.add(Box.createVerticalGlue());

        //set label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan MataKuliah");
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);

        box.add(Box.createVerticalStrut(15));

        //set Label "Nama mata kuliah: "
        JLabel labelNama = new JLabel();
        labelNama.setText("Nama: " + mataKuliah.getNama());
        labelNama.setForeground(SistemAkademikGUI.fontColor);
        labelNama.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNama);

        box.add(Box.createVerticalStrut(15));

        //set Label "Kode: "
        JLabel labelKode = new JLabel();
        labelKode.setText("Kode: " + mataKuliah.getKode());
        labelKode.setForeground(SistemAkademikGUI.fontColor);
        labelKode.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelKode.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelKode);

        box.add(Box.createVerticalStrut(15));

        //set Label "SKS: "
        JLabel labelSKS = new JLabel();
        labelSKS.setText("SKS: " + mataKuliah.getSKS());
        labelSKS.setForeground(SistemAkademikGUI.fontColor);
        labelSKS.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelSKS.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelSKS);

        box.add(Box.createVerticalStrut(15));

        //set Label "Jumlah mahasiswa: "
        JLabel labelJumlahMahasiswa = new JLabel();
        labelJumlahMahasiswa.setText("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        labelJumlahMahasiswa.setForeground(SistemAkademikGUI.fontColor);
        labelJumlahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelJumlahMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelJumlahMahasiswa);

        box.add(Box.createVerticalStrut(15));

        //set Label "Kapasitas: "
        JLabel labelKapasitas = new JLabel();
        labelKapasitas.setText("Kapasitas: " + mataKuliah.getKapasitas());
        labelKapasitas.setForeground(SistemAkademikGUI.fontColor);
        labelKapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelKapasitas.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelKapasitas);

        box.add(Box.createVerticalStrut(15));

        //set Label "Daftar Mahasiswa:"
        JLabel labelDaftarMatkul = new JLabel();
        labelDaftarMatkul.setText("Daftar Mahasiswa:");
        labelDaftarMatkul.setForeground(SistemAkademikGUI.fontColor);
        labelDaftarMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelDaftarMatkul.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelDaftarMatkul);

        box.add(Box.createVerticalStrut(15));

        //set daftar Mahasiswa yang mengambil MataKuliah ini
        if (!mataKuliahKosong(mataKuliah.getDaftarMahasiswa())) {
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
                JLabel labelMahasiswa = new JLabel();
                labelMahasiswa.setText((i+1) + ". " + mataKuliah.getDaftarMahasiswa()[i].getNama());
                labelMahasiswa.setForeground(SistemAkademikGUI.fontColor);
                labelMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
                labelMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
                box.add(labelMahasiswa);

                box.add(Box.createVerticalStrut(15));
            }
        } else {
            JLabel labelMahasiswa = new JLabel();
            labelMahasiswa.setText("Belum ada mahasiswa yang mengambil mata kuliah ini");
            labelMahasiswa.setForeground(SistemAkademikGUI.fontColor);
            labelMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
            labelMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
            box.add(labelMahasiswa);

            box.add(Box.createVerticalStrut(15));
        }

        //set button Selesai
        JButton btnSelesai = new JButton("Selesai");
        btnSelesai.setFont(SistemAkademikGUI.fontGeneral);
        btnSelesai.setBackground(SistemAkademikGUI.blueButton);
        btnSelesai.setForeground(SistemAkademikGUI.fontColor);
        btnSelesai.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });
        btnSelesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnSelesai);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan DetailRingkasaMataKuliahGUI ke frame SistemAkademik GUI
        frame.add(box);
    }

    /* method untuk cek apakah array Mahasiswa[] atribut Objek Mata Kuliah kosong */
    private static boolean mataKuliahKosong(Mahasiswa[] arr) {
        boolean kosong = true;
        for (int i = 0 ; i < arr.length ; i++){
            if (arr[i] != null){ //cek apakah terdapat objek mahasiswa di dalam array daftarMataKuliah
                kosong = false;
                break;
            }
        }
        return kosong;
    }
}

