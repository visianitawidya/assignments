package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Ringkasan Mahasiswa
        //set layout box
        Box box = Box.createVerticalBox();
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        box.add(Box.createVerticalGlue());

        //set label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mahasiswa");
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);

        box.add(Box.createVerticalStrut(15));

        //set "Pilih NPM"
        JLabel labelNPM = new JLabel();
        labelNPM.setText("Pilih NPM");
        labelNPM.setForeground(SistemAkademikGUI.fontColor);
        labelNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNPM);

        box.add(Box.createVerticalStrut(15));

        //set Drop Down Pilih NPM
        JComboBox mahasiswaComboBox = new JComboBox();
        mahasiswaComboBox.setMaximumSize(new Dimension(200, 160));

        if (!daftarMahasiswa.isEmpty()) { //cek apakah daftarMahasiswa sudah ada isi
            //mengubah ArrayList daftarMahasiswa menjadi array Mahasiswa[]
            Mahasiswa[] daftarMahasiswaArr = daftarMahasiswa.toArray(new Mahasiswa[daftarMahasiswa.size()]);
            //melakukan pengurutan berdasarkan nilai atribut NPM (terkecil - terbesar)
            Mahasiswa[] sortedNPM = sortNPM(daftarMahasiswaArr);
            //memasukkan atribut NPM tiap objek Mahasiswa (yang sudah terurut) ke dalam comboBox
            for (Mahasiswa mahasiswa : sortedNPM) {
                mahasiswaComboBox.addItem(mahasiswa.getNpm());
            }
        } else {
            mahasiswaComboBox.addItem(""); //jika daftarMahasiswa belum ada isi
        }
        mahasiswaComboBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(mahasiswaComboBox);

        box.add(Box.createVerticalStrut(15));

        //membuat button Lihat
        JButton btnLihat = new JButton("Lihat");
        btnLihat.setFont(SistemAkademikGUI.fontGeneral);
        btnLihat.setBackground(SistemAkademikGUI.orangeButton);
        btnLihat.setForeground(Color.white);
        btnLihat.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (mahasiswaComboBox.getSelectedItem().equals("")) {
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh field");
                } else {
                    frame.remove(box);
                    Mahasiswa mahasiswa = getMahasiswa((Long)mahasiswaComboBox.getSelectedItem(), daftarMahasiswa);
                    mahasiswa.cekIRS();
                    new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                    frame.revalidate();
                }
            }
        });
        btnLihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnLihat);

        box.add(Box.createVerticalStrut(15));

        //set button kembali
        JButton btnKembali = new JButton("Kembali");
        btnKembali.setFont(SistemAkademikGUI.fontGeneral);
        btnKembali.setBackground(SistemAkademikGUI.blueButton);
        btnKembali.setForeground(Color.white);
        btnKembali.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });
        btnKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnKembali);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan RingkasanMahasiswaGUI ke frame SistemAkademik GUI
        frame.add(box);
    }

    /* method untuk mengembalikan objek Mahasiswa tertentu pada daftarMahasiswa */
    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa){
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm) {
                return mahasiswa;
            }
        }
        return null;
    }

    /* method untuk sort array Mahasiswa[] berdasarkan nilai dari NPM (terkecil - terbesar) */
    private Mahasiswa[] sortNPM(Mahasiswa[] arr){
        int totalMahasiswa = arr.length;
        for (int i = 0; i < totalMahasiswa - 1; i++) {
            for (int j = i + 1; j < totalMahasiswa; j++) {
                //membandingkan tiap elemen pada array
                if (arr[i].getNpm() - (arr[j].getNpm()) > 0) {
                    //menukar elemen agar sesuai urutan
                    Mahasiswa temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }
}
