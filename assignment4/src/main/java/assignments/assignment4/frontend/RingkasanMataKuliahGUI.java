package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Ringkasan Mata Kuliah
        //set layout box
        Box box = Box.createVerticalBox();
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        box.add(Box.createVerticalGlue());

        //set label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mata Kuliah");
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);

        box.add(Box.createVerticalStrut(15));

        //set "Pilih Nama Matkul"
        JLabel labelNamaMatkul = new JLabel();
        labelNamaMatkul.setText("Pilih Nama Matkul");
        labelNamaMatkul.setForeground(SistemAkademikGUI.fontColor);
        labelNamaMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNamaMatkul.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNamaMatkul);

        box.add(Box.createVerticalStrut(15));

        //set Drop Down Pilih Mata Kuliah
        JComboBox matkulComboBox = new JComboBox();
        matkulComboBox.setMaximumSize(new Dimension(200, 160));

        if (!daftarMataKuliah.isEmpty()) { //cek apakah daftarMataKuliah sudah ada isi
            //mengubah ArrayList daftarMataKuliah menjadi array MataKuliah[]
            MataKuliah[] daftarMatkulArr = daftarMataKuliah.toArray(new MataKuliah[daftarMataKuliah.size()]);
            //melakukan pengurutan berdasarkan nilai atribut Nama secara alfabetik
            MataKuliah[] sortedMatkul = sortMatkul(daftarMatkulArr);
            //memasukkan atribut nama tiap objek MataKuliah (yang sudah terurut) ke dalam comboBox
            for (MataKuliah mataKuliah : sortedMatkul) {
                matkulComboBox.addItem(mataKuliah.getNama());
            }
        } else {
            matkulComboBox.addItem(""); //jika daftarMataKuliah belum ada isi
        }
        matkulComboBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(matkulComboBox);

        box.add(Box.createVerticalStrut(15));

        //membuat button Lihat
        JButton btnLihat = new JButton("Lihat");
        btnLihat.setFont(SistemAkademikGUI.fontGeneral);
        btnLihat.setBackground(SistemAkademikGUI.orangeButton);
        btnLihat.setForeground(Color.white);
        btnLihat.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (matkulComboBox.getSelectedItem().equals("")) {
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh field");
                } else {
                    frame.remove(box);
                    MataKuliah mataKuliah = getMataKuliah((String) matkulComboBox.getSelectedItem(), daftarMataKuliah);
                    new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah);
                    frame.revalidate();
                }
            }
        });
        btnLihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnLihat);

        box.add(Box.createVerticalStrut(15));

        //set button kembali
        JButton btnKembali = new JButton("Kembali");
        btnKembali.setFont(SistemAkademikGUI.fontGeneral);
        btnKembali.setBackground(SistemAkademikGUI.blueButton);
        btnKembali.setForeground(Color.white);
        btnKembali.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });
        btnKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnKembali);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan RingkasanMataKuliahGUI ke frame SistemAkademik GUI
        frame.add(box);
    }

    /* method untuk mengembalikan objek MataKuliah tertentu pada daftarMataKuliah */
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah){

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)) {
                return mataKuliah;
            }
        }
        return null;
    }

    /* method untuk sort array Matakuliah[] berdasarkan nilai atribut Nama objek MataKuliah secara alfabetik */
    private MataKuliah[] sortMatkul(MataKuliah[] arr){
        int total = arr.length;
        for (int i = 0; i < total - 1; i++) {
            for (int j = i + 1; j < total; j++) {
                //membandingkan tiap elemen pada array
                if (arr[i].getNama().compareTo(arr[j].getNama()) > 0) {
                    //menukar elemen agar sesuai urutan
                    MataKuliah temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }
}
