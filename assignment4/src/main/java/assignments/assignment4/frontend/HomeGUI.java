package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI extends JFrame{
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Halaman Home
        //buat layout box
        Box box = Box.createVerticalBox();
        box.add(Box.createVerticalGlue());
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        //label Judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);
        box.add(Box.createVerticalStrut(15));

        //button Tambah Mahasiswa
        JButton btnTambahMahasiswa = new JButton("Tambah Mahasiswa");
        btnTambahMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        btnTambahMahasiswa.setMaximumSize(new Dimension(200, 170));
        btnTambahMahasiswa.setBackground(SistemAkademikGUI.orangeButton);
        btnTambahMahasiswa.setForeground(Color.WHITE);
        btnTambahMahasiswa.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });

        btnTambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnTambahMahasiswa);
        box.add(Box.createVerticalStrut(15));

        //membuat button Tambah Mata Kuliah
        JButton btnTambahMatkul = new JButton("Tambah Mata Kuliah");
        btnTambahMatkul.setFont(SistemAkademikGUI.fontGeneral);
        btnTambahMatkul.setMaximumSize(new Dimension(200, 170));
        btnTambahMatkul.setBackground(SistemAkademikGUI.orangeButton);
        btnTambahMatkul.setForeground(Color.WHITE);
        btnTambahMatkul.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });

        btnTambahMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnTambahMatkul);
        box.add(Box.createVerticalStrut(15));

        //membuat button Tambah IRS
        JButton btnTambahIRS = new JButton("Tambah IRS");
        btnTambahIRS.setFont(SistemAkademikGUI.fontGeneral);
        btnTambahIRS.setMaximumSize(new Dimension(200, 170));
        btnTambahIRS.setBackground(SistemAkademikGUI.orangeButton);
        btnTambahIRS.setForeground(Color.WHITE);
        btnTambahIRS.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });

        btnTambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnTambahIRS);
        box.add(Box.createVerticalStrut(15));

        //membuat button Hapus IRS
        JButton btnHapusIRS = new JButton("Hapus IRS");
        btnHapusIRS.setFont(SistemAkademikGUI.fontGeneral);
        btnHapusIRS.setMaximumSize(new Dimension(200, 170));
        btnHapusIRS.setBackground(SistemAkademikGUI.orangeButton);
        btnHapusIRS.setForeground(Color.WHITE);
        btnHapusIRS.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });

        btnHapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnHapusIRS);
        box.add(Box.createVerticalStrut(15));

        //membuat button Ringkasan Mahasiswa
        JButton btnRingkasanMahasiswa = new JButton("Ringkasan Mahasiswa");
        btnRingkasanMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        btnRingkasanMahasiswa.setMaximumSize(new Dimension(200, 170));
        btnRingkasanMahasiswa.setBackground(SistemAkademikGUI.orangeButton);
        btnRingkasanMahasiswa.setForeground(Color.WHITE);
        btnRingkasanMahasiswa.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });

        btnRingkasanMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnRingkasanMahasiswa);
        box.add(Box.createVerticalStrut(15));

        //membuat button Ringkasan Mata Kuliah
        JButton btnRingkasanMataKuliah = new JButton("Ringkasan MataKuliah");
        btnRingkasanMataKuliah.setFont(SistemAkademikGUI.fontGeneral);
        btnRingkasanMataKuliah.setMaximumSize(new Dimension(200, 170));
        btnRingkasanMataKuliah.setBackground(SistemAkademikGUI.orangeButton);
        btnRingkasanMataKuliah.setForeground(Color.WHITE);
        btnRingkasanMataKuliah.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });

        btnRingkasanMataKuliah.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnRingkasanMataKuliah);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan HomeGUI ke frame SistemAkademik GUI
        frame.add(box);
    }
}
