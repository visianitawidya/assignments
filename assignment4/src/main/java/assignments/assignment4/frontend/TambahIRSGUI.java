package assignments.assignment4.frontend;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI extends JFrame {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        // TODO: Implementasikan Tambah IRS
        //set layout box
        Box box = Box.createVerticalBox();
        box.setBackground(SistemAkademikGUI.bgColor);
        box.setOpaque(true);

        box.add(Box.createVerticalGlue());

        //set label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah IRS");
        titleLabel.setForeground(SistemAkademikGUI.fontColor);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        box.add(titleLabel);

        box.add(Box.createVerticalStrut(15));

        //set "Pilih NPM"
        JLabel labelNPM = new JLabel();
        labelNPM.setText("Pilih NPM");
        labelNPM.setForeground(SistemAkademikGUI.fontColor);
        labelNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelNPM);

        box.add(Box.createVerticalStrut(15));

        //set Drop Down Pilih NPM
        JComboBox mahasiswaComboBox = new JComboBox();
        mahasiswaComboBox.setMaximumSize(new Dimension(200, 160));

        if (!daftarMahasiswa.isEmpty()) { //cek apakah daftarMahasiswa sudah ada isi
            //mengubah ArrayList daftarMahasiswa menjadi array Mahasiswa[]
            Mahasiswa[] daftarMahasiswaArr = daftarMahasiswa.toArray(new Mahasiswa[daftarMahasiswa.size()]);
            //melakukan pengurutan berdasarkan nilai atribut NPM (terkecil - terbesar)
            Mahasiswa[] sortedNPM = sortNPM(daftarMahasiswaArr);
            //memasukkan atribut NPM tiap objek Mahasiswa (yang sudah terurut) ke dalam comboBox
            for (Mahasiswa mahasiswa : sortedNPM) {
                mahasiswaComboBox.addItem(mahasiswa.getNpm());
            }
        } else {
            mahasiswaComboBox.addItem(""); //jika daftarMahasiswa belum ada isi
        }
        mahasiswaComboBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(mahasiswaComboBox);

        box.add(Box.createVerticalStrut(15));

        //set "Pilih Nama Mata Kuliah"
        JLabel labelMatkul = new JLabel();
        labelMatkul.setText("Pilih Nama Mata Kuliah");
        labelMatkul.setForeground(SistemAkademikGUI.fontColor);
        labelMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelMatkul.setFont(SistemAkademikGUI.fontGeneral);
        box.add(labelMatkul);

        box.add(Box.createVerticalStrut(15));

        //set Drop Down nama mata kuliah
        JComboBox matkulComboBox = new JComboBox();
        matkulComboBox.setMaximumSize(new Dimension(200, 160));

        if (!daftarMataKuliah.isEmpty()) { //cek apakah daftarMataKuliah sudah ada isi
            //mengubah ArrayList daftarMataKuliah menjadi array MataKuliah[]
            MataKuliah[] daftarMatkulArr = daftarMataKuliah.toArray(new MataKuliah[daftarMataKuliah.size()]);
            //melakukan pengurutan berdasarkan nilai atribut Nama secara alfabetik
            MataKuliah[] sortedMatkul = sortMatkul(daftarMatkulArr);
            //memasukkan atribut nama tiap objek MataKuliah (yang sudah terurut) ke dalam comboBox
            for (MataKuliah mataKuliah : sortedMatkul) {
                matkulComboBox.addItem(mataKuliah.getNama());
            }
        } else {
            matkulComboBox.addItem(""); //jika daftarMataKuliah belum ada isi
        }
        matkulComboBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(matkulComboBox);

        box.add(Box.createVerticalStrut(15));

        //set button "Tambahkan"
        JButton btnTambahMatkul = new JButton("Tambahkan");
        btnTambahMatkul.setFont(SistemAkademikGUI.fontGeneral);
        btnTambahMatkul.setBackground(SistemAkademikGUI.orangeButton);
        btnTambahMatkul.setForeground(Color.white);
        btnTambahMatkul.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                //cek apakah ada yang dipilih dari combobox
                if (mahasiswaComboBox.getSelectedItem().equals("") || matkulComboBox.getSelectedItem().equals("")) {
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh field");
                } else {
                    //melakukan menambahan IRS pada objek Mahasiswa dan menampilkan OptionPane
                    Mahasiswa mahasiswa = getMahasiswa((Long)mahasiswaComboBox.getSelectedItem(), daftarMahasiswa);
                    MataKuliah mataKuliah = getMataKuliah((String)matkulComboBox.getSelectedItem(), daftarMataKuliah);
                    String message = mahasiswa.addMatkul(mataKuliah);
                    JOptionPane.showMessageDialog(null, message);
                }
            }
        });
        btnTambahMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnTambahMatkul);

        box.add(Box.createVerticalStrut(15));

        //set button kembali
        JButton btnKembali = new JButton("Kembali");
        btnKembali.setFont(SistemAkademikGUI.fontGeneral);
        btnKembali.setBackground(SistemAkademikGUI.blueButton);
        btnKembali.setForeground(Color.white);
        btnKembali.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.remove(box);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
            }
        });
        btnKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(btnKembali);

        box.add(Box.createVerticalStrut(15));
        box.add(Box.createVerticalGlue());

        //menambahkan TambahIRSGUI ke frame SistemAkademik GUI
        frame.add(box);
        }

        /* method untuk mengembalikan objek MataKuliah tertentu pada daftarMataKuliah */
        private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah){

            for (MataKuliah mataKuliah : daftarMataKuliah) {
                if (mataKuliah.getNama().equals(nama)) {
                    return mataKuliah;
                }
            }
            return null;
        }

        /* method untuk mengembalikan objek Mahasiswa tertentu pada daftarMahasiswa */
        private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa){
            for (Mahasiswa mahasiswa : daftarMahasiswa) {
                if (mahasiswa.getNpm() == npm) {
                    return mahasiswa;
                }
            }
            return null;
        }

        /* method untuk sort array Mahasiswa[] berdasarkan nilai dari NPM (terkecil - terbesar) */
        private Mahasiswa[] sortNPM(Mahasiswa[] arr){
            int totalMahasiswa = arr.length;
            for (int i = 0; i < totalMahasiswa - 1; i++) {
                for (int j = i + 1; j < totalMahasiswa; j++) {
                    //membandingkan tiap elemen pada array
                    if (arr[i].getNpm() - (arr[j].getNpm()) > 0) {
                        //menukar elemen agar sesuai urutan
                        Mahasiswa temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }

        /* method untuk sort array Matakuliah[] berdasarkan nilai atribut Nama objek MataKuliah secara alfabetik */
        private MataKuliah[] sortMatkul(MataKuliah[] arr){
            int total = arr.length;
            for (int i = 0; i < total - 1; i++) {
                for (int j = i + 1; j < total; j++) {
                    //membandingkan tiap elemen pada array
                    if (arr[i].getNama().compareTo(arr[j].getNama()) > 0) {
                        //menukar elemen agar sesuai urutan
                        MataKuliah temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }
}

