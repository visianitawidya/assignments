package assignments.assignment1;

/*
 * TP 1 - NPM Extractor
 * Nama: Visianita Widyaningrum
 * NPM: 2006596610
 */

import java.util.Scanner;

public class ExtractNPM {
    /* Method untuk validasi NPM */
    public static boolean validate(long npm) {
        String npmString = Long.toString(npm);
        if (npmString.length() == 14){ //validasi panjang NPM
            // inisialisasi bagian dari struktur npm
            int tahunMasuk = Integer.parseInt(npmString.substring(0, 2));
            int kodeJurusan = Integer.parseInt(npmString.substring(2, 4));
            long tanggalLahir = Long.parseLong(npmString.substring(4, 12));
            int kodeRandom = Integer.parseInt(npmString.substring(13)); 

            // cek apakah syarat memenuhi
            if (cekTahun(tahunMasuk) && cekJurusan(kodeJurusan) && cekUmur(tanggalLahir) 
                && cekKode(kodeRandom,npmString)){
                return true;
            } 
        }
       return false;
    }

    /* Method untuk ekstraksi NPM */
    public static String extract(long npm) {
        String npmString = Long.toString(npm);
        // inisialisasi bagian dari struktur npm
        String tahunMasuk = npmString.substring(0, 2);
        int kodeJurusan = Integer.parseInt(npmString.substring(2, 4));
        String tanggalLahir = npmString.substring(4, 12);

        // inisialisasi isi dari output
        String res = "Tahun masuk: 20" + tahunMasuk + '\n';
        res += "Jurusan: " + infoJurusan(kodeJurusan) + '\n';
        res += "Tanggal Lahir: " + tanggalLahir.substring(0,2) + "-" + tanggalLahir.substring(2,4) + "-" + tanggalLahir.substring(4);
        return res;
    }

    /* Method untuk validasi tahun masuk */
    public static boolean cekTahun(int tahun){
        if (tahun >= 10){
            return true; // tahun masuk harus setelah 2010
        }
        return false;
    }

    /* Method untuk validasi kode jurusan */
    public static boolean cekJurusan(int kode){
        if (kode == 01 || kode == 02 || kode == 03 || kode == 11 || kode == 12){
            return true; //jika memenuhi syarat kode jurusan yang sudah ada
        }
        return false;
    }

    /* Method untuk validasi tanggal lahir */
    public static boolean cekUmur(long tahun){
        int umurMinimal = 15; // usia minimal menjadi mahasiswa 15 tahun
        int tahunSekarang = 2021; 
        if (tahunSekarang - tahun < umurMinimal){ 
            return true; // jika selisih tahun lahir dan tahun sekarang melebihi umur minimal
        }
        return false;
    }

    /* Method untuk validasi tanggal kode */
    public static boolean cekKode(int kode, String npm){
        int res = 0;
        // mengalikan digit pertama dan digit ke-13
        // menjumlahkan dengan perkalian digit i++ dari depan dan i-- dari belakang hingga i sebelum digit tengah
        for (int i = 0; i < npm.length() / 2; i++){
            int left = npm.charAt(i) - '0';
            int right = npm.charAt(npm.length() - i - 2) - '0';
            res += left * right;
        }
        // menjumlahkan digit tengah ke res
        res += (npm.charAt(npm.length()/2 - 1) - '0');
        int sum = 0;
        // melakukan penjumlahan digit yang ada jika terdapat lebih dari 1 digit
        while (res > 0 || sum > 9){
            if (res == 0){ 
                res = sum;
                sum = 0; //jika jumlahnya 0
            }
            sum += res % 10;  // melakukann penjumlahan dari digit terbelakang
            res /= 10;
        }
        return sum == kode;
    }

    /* Method untuk mencari jurusan berdasarkan kode jurusan */
    public static String infoJurusan(int kode){
        switch(kode){
            case 01: return "Ilmu Komputer";
            case 02: return "Sistem Informasi";
            case 03: return "Teknologi Informasi";
            case 11: return "Teknik Telekomunikasi";
            case 12: return "Teknik Elektro";
            default: return "";
        }
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            } else {
                if (validate(npm)){
                    System.out.println(extract(npm));
                } else {
                    System.out.println("NPM tidak valid!");
                }
            }
        }
        input.close();  
    }
}