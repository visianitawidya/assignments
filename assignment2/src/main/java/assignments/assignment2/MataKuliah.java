package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int totalMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        if (totalMahasiswa < this.kapasitas) {
            daftarMahasiswa[totalMahasiswa++] = mahasiswa;
        }
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // menncari index mahasiswa
        int index = 0;
        for (int i = 0; i < daftarMahasiswa.length; i++){
            if (daftarMahasiswa[i] == mahasiswa) {
                index = i;
            }
        }

        // menghilangkan elemen pada index mahasiswa yang akan dihilangkan
        for (int i = index; i < daftarMahasiswa.length - 1; i++) {
            daftarMahasiswa[i] = daftarMahasiswa[i + 1]; // elemen index + 1 dimajukan
        }

        totalMahasiswa--;
    }

    public String toString() {
        return this.nama;
    }

    /* getter */
    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public String getKode() {
        return kode;
    }

    public String getNama() {
        return nama;
    }

    public int getSks() {
        return sks;
    }

    public int getTotalMahasiswa() {
        return totalMahasiswa;
    }
}
