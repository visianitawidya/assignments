package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalMahasiswa;
    private static int totalMataKuliah;
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i].getNpm() == npm) {
                return daftarMahasiswa[i];
            }
        }             
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i].getNama().equals(namaMataKuliah)) {
                return daftarMataKuliah[i];
            }
        }   
        return null; 
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        // mengambil objek Mahasiswa dari daftarMahasiswa
        Mahasiswa mahasiswa = getMahasiswa(npm);

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            // menambahkan matkul ke mataKuliah milik objek mahasiswa
            mahasiswa.addMatkul(getMataKuliah(namaMataKuliah));
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        // mengambil objek Mahasiswa dari daftarMahasiswa
        Mahasiswa mahasiswa = getMahasiswa(npm);
        if (mahasiswa.getTotalMatkul() == 0 ) { // jika mahasiswa belum ambil matkul
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        } else {
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++) 
            {
                System.out.print("Nama matakuliah " + (i+1) + " : ");
                String namaMataKuliah = input.nextLine();
                // menghapus matkul dari mataKuliah milik objek mahasiswa
                mahasiswa.dropMatkul(getMataKuliah(namaMataKuliah));
                System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
            }
        } 
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa.getNama());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        if (mahasiswa.getTotalMatkul() == 0) {
            System.out.println("Belum ada mata kuliah yang diambil");
        } else {
            for (int i = 0; i < mahasiswa.getTotalMatkul(); i++){
                System.out.println((i + 1) + ". " + mahasiswa.getMataKuliah()[i].getNama());
            }
        }

        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");
        mahasiswa.cekIRS();
        if (mahasiswa.getTotalMasalahIRS() == 0) {
            System.out.println("IRS tidak bermasalah.");
        } else {
            for (int i = 0; i < mahasiswa.getTotalMasalahIRS(); i++){
                System.out.println((i + 1) + ". " + mahasiswa.getMasalahIRS()[i]);
            }
        }
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + mataKuliah.getNama());
        System.out.println("Kode: " + mataKuliah.getKode());
        System.out.println("SKS: " + mataKuliah.getSks());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getTotalMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
        if (mataKuliah.getTotalMahasiswa() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        } else {
            for (int i = 0; i < mataKuliah.getTotalMahasiswa(); i++){
                System.out.println((i + 1) + "." + mataKuliah.getDaftarMahasiswa()[i]);
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            MataKuliah mataKuliah = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
            daftarMataKuliah[totalMataKuliah++] = mataKuliah;
        }


        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            Mahasiswa mahasiswa = new Mahasiswa(dataMahasiswa[0], npm);
            daftarMahasiswa[totalMahasiswa++] = mahasiswa;
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
