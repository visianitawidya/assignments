package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    private int totalMatkul;
    private int totalMasalahIRS;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.masalahIRS = new String[20];

        String npmString = Long.toString(npm);
        String kodeJurusan = npmString.substring(2, 4);

        if (kodeJurusan.equals("01")) {
            this.jurusan = "Ilmu Komputer";
        } else if (kodeJurusan.equals("02")) {
            this.jurusan = "Sistem Informasi";
        }
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        // cek apakah matkul ada dalam array mataKuliah
        boolean adaMatkul = false;
        for (MataKuliah i : this.mataKuliah) {
            if (i == mataKuliah) {
                adaMatkul = true;
            }
        }
        
        if (adaMatkul) { // cek apakah matkul sudah ada dalam array mataKuliah
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah diambil sebelumnya.");
        } else {
            // cek kapasitas mata kuliah
            if (mataKuliah.getTotalMahasiswa() == mataKuliah.getKapasitas()) {
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah penuh kapasitasnya.");
            } else {
                if (totalMatkul == 10) { // cek apakah mahasiswa sudah mengambil 10 mata kuliah
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                } else {
                    this.mataKuliah[totalMatkul++] = mataKuliah; 
                    this.totalSKS += mataKuliah.getSks();
                    mataKuliah.addMahasiswa(this);
                }
            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        // cek apakah matkul ada dalam array mataKuliah
        boolean adaMatkul = false;
        for (MataKuliah i : this.mataKuliah) {
            if (i == mataKuliah) {
                adaMatkul = true;
            }
        }

        if (adaMatkul) {
            // menncari index mata kuliah
            int index = 0;
            for (int i = 0; i < this.mataKuliah.length; i++){
                if (this.mataKuliah[i] == mataKuliah) {
                    index = i;
                }
            }
            // menghilangkan elemen pada index mahasiswa yang akan dihilangkan
            for (int i = index; i < this.mataKuliah.length - 1; i++) {
                this.mataKuliah[i] = this.mataKuliah[i + 1]; // elemen index + 1 dimajukan
            }
            totalSKS -= mataKuliah.getSks();
            totalMatkul--;
            mataKuliah.dropMahasiswa(this);
        } else {
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " belum pernah diambil");
        }
    }


    public void cekIRS(){
        // cek apakah matkul yang diambil sudah sesuai jurusan
        for (int i = 0; i < mataKuliah.length; i++) {
            if (jurusan == "Ilmu Komputer") { // untuk mahasiswa ilkom
                if (mataKuliah[i] != null && mataKuliah[i].getKode().equals("SI")) {
                    String warning2 = "Mata Kuliah " + mataKuliah[i].getNama() + " tidak dapat diambil jurusan IK";
                    if (!adaMasalah(warning2)) {
                    masalahIRS[totalMasalahIRS++] = warning2;
                    }
                }
            } else if (jurusan == "Sistem Informasi") { // untuk mahasiswa SI
                if (mataKuliah[i] != null && mataKuliah[i].getKode().equals("IK")) {
                    String warning3 = "Mata Kuliah " + mataKuliah[i].getNama() + " tidak dapat diambil jurusan SI";
                    if (!adaMasalah(warning3)) {
                        masalahIRS[totalMasalahIRS++] = warning3;
                    }
                }
            }
        }
        //cek apakah jumlah SKS tidak melebihi 24 
        if (totalSKS > 24){
            String warning = "SKS yang Anda ambil lebih dari 24";
            if (!adaMasalah(warning)) {
                masalahIRS[totalMasalahIRS++] = warning;
            } 
        }
    }

    public boolean adaMasalah(String warning) {
        boolean ada = false;
        for (int i = 0; i < masalahIRS.length; i++) {
            if (masalahIRS[i] != null && masalahIRS[i].equals(warning)) {
                ada = true;
            }
        }
        return ada;
    }

    public String toString() {
        return this.nama;
    }

    /* getter */
    public String getNama() {
        return nama;
    }

    public String getJurusan() {
        return jurusan;
    }

    public String[] getMasalahIRS() {
        return masalahIRS;
    }

    public MataKuliah[] getMataKuliah() {
        return mataKuliah;
    }

    public long getNpm() {
        return npm;
    }

    public int getTotalSKS() {
        return totalSKS;
    }

    public int getTotalMasalahIRS() {
        return totalMasalahIRS;
    }

    public int getTotalMatkul() {
        return totalMatkul;
    }
}
