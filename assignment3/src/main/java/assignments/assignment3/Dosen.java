package assignments.assignment3;

class Dosen extends ElemenFasilkom {
    private MataKuliah mataKuliah;

    /* Konstruktor */
    Dosen(String nama) {
        this.setNama(nama);
        this.setTipe("Dosen");
    }

    /* implementasi method ketika objek Dosen mengajar MataKuiliah */
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        //jika Dosen sudah memiliki atribut mataKuliah atau MataKuliah sudah punya Dosen
        //tidak dapat melakukan method mengajarMatakuliah
        if (this.mataKuliah != null) {
            System.out.println("[DITOLAK] " + this.getNama() + " sudah mengajar mata kuliah " + this.getMataKuliah());
        } else if (mataKuliah.getDosen() != null) {
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " sudah memiliki dosen pengajar");
        } else {
            this.mataKuliah = mataKuliah; //set atribut mataKuliah menjadi objek MataKuliah
            System.out.println(this.getNama() + " mengajar mata kuliah " + this.getMataKuliah());
            mataKuliah.addDosen(this); //mengimplementasikan method addDosen objek mataKuliah
        }
    }

    /* implementasi method ketika objek Dosen drop MataKuiliah */
    public void dropMataKuliah() {
        //jika Dosen tidak memiliki atribut mataKuliah
        //tidak dapat melakukan method dropMataKuliah
        if (this.mataKuliah == null) {
            System.out.println("[DITOLAK] " + this.getNama() + " sedang tidak mengajar mata kuliah apapun");
        } else {
            this.mataKuliah.dropDosen(); //mengimplementasikan method dropDosen objek mataKuliah
            this.mataKuliah = null; //set atribut mataKuliah menjadi objek null
        }
    }

    /* getter */
    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }
}