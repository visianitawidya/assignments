package assignments.assignment3;

class MataKuliah {
    private String nama;
    private int kapasitas;
    private Dosen dosen;
    private Mahasiswa[] daftarMahasiswa;
    private int totalMahasiswa;

    /* Konstruktor */
    MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    /* Method untuk menambahkan objek Mahasiswa ke array daftarMahasiswa */
    public void addMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0 ; i < this.kapasitas ; i++) {
            // mengganti objek null yang ditemukan pertama kali dengan objek Mahasiswa
            if (this.daftarMahasiswa[i] == null){
                this.daftarMahasiswa[i] = mahasiswa;
                this.totalMahasiswa++;
                break;
            }
        }
    }

    /* Method untuk menghapus objek Mahasiswa dari array daftarMahasiswa */
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // mencari index Mahasiswa
        int index = 0;
        for (int i = 0; i < daftarMahasiswa.length; i++){
            if (daftarMahasiswa[i] == mahasiswa) {
                index = i;
            }
        }

        // menghilangkan elemen pada index Mahasiswa yang akan dihilangkan
        for (int i = index; i < daftarMahasiswa.length - 1; i++) {
            daftarMahasiswa[i] = daftarMahasiswa[i + 1]; // elemen (index + 1) dimajukan
        }
        totalMahasiswa--;
    }

    /* Method untuk menambahkan objek Dosen ke atribut dosen */
    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    /* Method untuk menghapus objek Dosen dari atribut dosen */
    public void dropDosen() {
        this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }

    /* method tambahan */
    /* method untuk cek apakah kapasitas mataKuliah penuh */
    public boolean kapasitasPenuh() {
        //jika jumlah Mahasiswa sama dengan kapasitasnya, kapasitas sudah penuh
        if (this.getTotalMahasiswa() == this.getKapasitas()) {
            return true;
        } else {return false;}
    }

    /* getter */

    public String getNama() {
        return nama;
    }

    public int getTotalMahasiswa() {
        return totalMahasiswa;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public Dosen getDosen() {
        return dosen;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }
}