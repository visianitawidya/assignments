package assignments.assignment3;

class Makanan {
    private String nama;
    private long harga;

    /* Konstruktor */
    Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    public String toString() {
        return this.nama;
    }

    /* getter */

    public long getHarga() {
        return harga;
    }

    public String getNama() {
        return nama;
    }
}