package assignments.assignment3;

abstract class ElemenFasilkom {
    private String tipe;
    private String nama;
    private int friendship;
    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    /* method untuk melakukan aktivitas menyapa */
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        boolean sudahMenyapa = sudahMenyapa(elemenFasilkom); //cek apakah kedua objek ElemenFasilkom telah bersapa sebelumnya
        if (!sudahMenyapa) {
            //menambahkan objek ElemenFasilkom (elemenFasilkom) ke dalam array atribut telahMenyapa milik this
            for (int i = 0 ; i < this.telahMenyapa.length ; i++) {
                if (this.telahMenyapa[i] == null) {
                    this.telahMenyapa[i] = elemenFasilkom;
                    System.out.println(this.nama + " menyapa dengan " + elemenFasilkom.getNama());
                    break;
                }
            }

            //menambahkan this ke dalam array atribut telahMenyapa milik objek ElemenFasilkom (elemenFasilkom)
            for (int i = 0 ; i < elemenFasilkom.telahMenyapa.length ; i++) {
                if (elemenFasilkom.telahMenyapa[i] == null) {
                    elemenFasilkom.telahMenyapa[i] = this;
                    break;
                }
            }

            //cek hubungan kedua objek untuk implementasi nilai atribut friendship
            cekHubunganMahasiswaDosen(this, elemenFasilkom);
        } else {
            //jika kedua ElemenFasilkom telah menyapa, tidak dapat melakukan method menyapa
            System.out.println("[DITOLAK] " + this + " telah menyapa " + elemenFasilkom + " hari ini");
        }
    }

    /* method untuk menghapus semua objek ElemenFasilkom dari array telahMenyapa */
    public void resetMenyapa() {
        for (int i = 0 ; i < this.telahMenyapa.length ; i++) {
            if (this.telahMenyapa[i] != null) {
                this.telahMenyapa[i] = null;
            }
        }
    }

    /* method untuk melakukan aktivitas membeliMakanan */
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        ElemenKantin penjualMakanan = (ElemenKantin)penjual;
        if (penjualMakanan.getDaftarMakanan() != null) {
           Makanan makanan = penjualMakanan.getMakanan(namaMakanan);
            if (penjualMakanan.sudahTerdaftar(makanan)) {
                System.out.println(pembeli.getNama() + " berhasil membeli " + namaMakanan + " seharga " + makanan.getHarga());
                //perhitungan atribut friendship
                //setiap ElemenFasilkom mendapat tambahan 1 nilai
                //cek nilai atribut friendship agar tidak melewati interval
                pembeli.setFriendship(pembeli.getFriendship() + 1);
                pembeli.cekFriendship();
                penjual.setFriendship(penjual.getFriendship() + 1);
                penjual.cekFriendship();
            } else {
                //jika Makanan belum ada pada array daftarMakanan milik ElemenKantin, tidak dapat melakukan method membeliMakanan
                System.out.println("[DITOLAK] " + penjualMakanan.getNama() + " tidak menjual " + namaMakanan);
            } 
        } else {
            //jika ElemenKantin tidak memiliki Makanan pada array daftarMakanan, tidak dapat melakukan method membeliMakanan
            System.out.println("[DITOLAK] " + penjualMakanan.getNama() + " tidak menjual " + namaMakanan);
        } 
        
    }

    public String toString() {
        return this.nama;
    }

    /* getter */

    public String getNama() {
        return nama;
    }

    public String getTipe() {
        return tipe;
    }

    public int getFriendship() {
        return friendship;
    }

    public ElemenFasilkom[] getTelahMenyapa() {
        return telahMenyapa;
    }

    /* setter */

    public void setNama(String nama) {
        this.nama = nama;
    }
    
    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public void setFriendship(int friendship) {
        this.friendship = friendship;
    }    

    /* method tambahan */

    /* method untuk cek apakah elemen tersebut sudah ada dalam array telahMenyapa */
    public boolean sudahMenyapa(ElemenFasilkom elemenFasilkom){
        boolean ada = false;
        for (int i = 0 ; i < this.telahMenyapa.length ; i++){
            if (this.telahMenyapa[i] != null){
                if(this.telahMenyapa[i].getNama().equals(elemenFasilkom.getNama())){
                    ada = true;
                    break;
                }
            }
        }
        return ada;   
    }

    /* method untuk menghitung berapa banyak objek ElemenFasilkom yang telah disapa */
    public int totalMenyapa() {
        int count = 0;
        for (ElemenFasilkom elemen : telahMenyapa) {
            //increment pada tiap elemen objek ElemenFasilkom pada atribut telahMenyapa
            if (elemen != null) count++; 
        }
        return count;   
    }

    /* method untuk validasi atribut friendship */
    public void cekFriendship() {
        //nilai atribut friendship harus berada pada interval 0 - 100
        if (this.friendship > 100) {
            //ubah nilai friendship = 100 jika perubahan nilai friendship > 100
            this.friendship = 100; 
        }
        if (this.friendship < 0) {
            //ubah nilai friendship = 0 jika perubahan nilai friendship < 0
            this.friendship = 0;
        }
    }

    /* method untuk cek hubungan mahasiswa dosen setelah melakukan method menyapa dan implementasi perhitungan atribut friendship */
    public void cekHubunganMahasiswaDosen(ElemenFasilkom elemen1, ElemenFasilkom elemen2) {
        //cek apakah kedua objek tersebut adalah Mahasiswa dan Dosen
        //jika elemen1 Mahasiswa dan elemen2 Dosen
        if (elemen1.getTipe().equals("Mahasiswa") && elemen2.getTipe().equals("Dosen")) {
            Mahasiswa mahasiswa = (Mahasiswa)elemen1;
            Dosen dosen = (Dosen)elemen2;
            for (int i = 0 ; i < mahasiswa.getDaftarMataKuliah().length ; i++) {
                //cek apakah ada mataKuliah yang diambil Mahasiswa mempunyai dosen objek Dosen tersebut
                //cek apakah ada mataKuliah Mahasiswa pada atribut daftarMataKuliah Mahasiswa yang sama dengan atribut mataKuliah Dosen
                if (mahasiswa.getDaftarMataKuliah()[i] != null && dosen.getMataKuliah() != null) {
                    if (mahasiswa.getDaftarMataKuliah()[i].equals(dosen.getMataKuliah())) {
                        //perhitungan atribut friendship
                        //Mahasiswa dan Dosen mendapat tambahan 2 nilai
                        //cek nilai atribut friendship agar tidak melewati interval
                        mahasiswa.setFriendship(mahasiswa.getFriendship()+2);
                        mahasiswa.cekFriendship();
                        dosen.setFriendship(dosen.getFriendship()+2);
                        dosen.cekFriendship();
                    }
                }    
            }   
        }  
        //jika elemen2 Mahasiswa dan elemen1 Dosen
        if (elemen2.getTipe().equals("Mahasiswa") && elemen1.getTipe().equals("Dosen")) {
            Mahasiswa mahasiswa = (Mahasiswa)elemen2;
            Dosen dosen = (Dosen)elemen1;
            for (int i = 0 ; i < mahasiswa.getDaftarMataKuliah().length ; i++) {
                //cek apakah ada mataKuliah yang diambil Mahasiswa mempunyai dosen objek Dosen tersebut
                //cek apakah ada mataKuliah Mahasiswa pada atribut daftarMataKuliah Mahasiswa yang sama dengan atribut mataKuliah Dosen
                if (mahasiswa.getDaftarMataKuliah()[i] != null && dosen.getMataKuliah() != null) {
                    if (mahasiswa.getDaftarMataKuliah()[i].equals(dosen.getMataKuliah())) {
                        //perhitungan atribut friendship
                        //Mahasiswa dan Dosen mendapat tambahan 2 nilai
                        //cek nilai atribut friendship agar tidak melewati interval
                        mahasiswa.setFriendship(mahasiswa.getFriendship()+2);
                        mahasiswa.cekFriendship();
                        dosen.setFriendship(dosen.getFriendship()+2);
                        dosen.cekFriendship();
                    }
                }    
            } 
        }
    }
}