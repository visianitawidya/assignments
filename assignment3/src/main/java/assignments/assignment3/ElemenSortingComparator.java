package assignments.assignment3;
import java.util.Comparator;
public class ElemenSortingComparator implements Comparator<ElemenFasilkom>{

    /* nilai dari method compare akan digunakan untuk mengurutkan array daftarElemenFasilkom pada Main */
    @Override
    public int compare(ElemenFasilkom elemen1, ElemenFasilkom elemen2) {
        if (elemen1 == null && elemen2 == null) {
            return 0;
        } else if (elemen1 == null){
            return 1;
        } else if (elemen2 == null){
            return -1;
        } else {
            //mencari selisih dari tiap atribut
            int compareName = elemen1.getNama().compareTo(elemen2.getNama());
            int compareFriendship = elemen2.getFriendship() - elemen1.getFriendship();
            //diurutkan dari friendship tertinggi lalu alphabetic
            if (compareFriendship == 0) {
                return compareName;
            } else {
                return compareFriendship;
            }
        }
    }
}
