package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private long npm;
    private String tanggalLahir;
    private String jurusan;

    /* Konstruktor */
    Mahasiswa(String nama, long npm) {
        this.setNama(nama);
        this.setTipe("Mahasiswa");
        this.npm = npm;
        this.jurusan = extractJurusan(npm);
        this.tanggalLahir = extractTanggalLahir(npm);
    }

    /* method untuk menambahkan Matkul yang dimiliki Mahasiswa */
    public void addMatkul(MataKuliah mataKuliah) {
        //jika mataKuliah sudah pernah didaftarkan atau kapasitas tidak mencukupi
        //tidak dapat melakukan method addMatkul
        if (sudahTerdaftar(mataKuliah)) {
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah diambil sebelumnya");
        } else if (mataKuliah.kapasitasPenuh()) {
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah penuh kapasitasnya");
        } else {
            //memasukkan objek MataKuliah ke dalam array daftarMatakuliah
            for (int i = 0 ; i < this.daftarMataKuliah.length ; i++){
                if (this.daftarMataKuliah[i] == null){ 
                    // mengganti objek null yang ditemukan pertama kali dengan objek MataKuliah
                    this.daftarMataKuliah[i] = mataKuliah;
                    mataKuliah.addMahasiswa(this); //mengimplementasikan method addMahasiswa objek mataKuliah
                    break; 
                }
            }
            System.out.println(this.getNama() + " berhasil menambahkan mata kuliah " + mataKuliah.getNama());
        }
    }

    /* method untuk menghilangkan suatu Matkul yang dimiliki Mahasiswa */
    public void dropMatkul(MataKuliah mataKuliah) {
        //jika MataKuliah belum pernah didaftarkan, tidak dapat melakukan method dropMatkul
        if (sudahTerdaftar(mataKuliah)) {
            // mencari index mataKuliah pada array daftarMataKuliah
            int index = 0;
            for (int i = 0; i < this.daftarMataKuliah.length; i++){
                if (this.daftarMataKuliah[i] == mataKuliah) {
                    index = i;
                }
            }
            // menghilangkan elemen pada index MataKuliah yang akan dihilangkan
            for (int i = index; i < this.daftarMataKuliah.length - 1; i++) {
                this.daftarMataKuliah[i] = this.daftarMataKuliah[i + 1]; // elemen (index + 1) dimajukan
            }
            mataKuliah.dropMahasiswa(this); //mengimplementasikan method dropMahasiswa objek mataKuliah
            System.out.println(this.getNama() + " berhasil drop mata kuliah " + mataKuliah.getNama());
        } else {
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " belum pernah diambil");
        }
    }

    /* method untuk mengekstrak tanggal lahir Mahasiswa dari npm */
    public String extractTanggalLahir(long npm) {
        String npmString = Long.toString(npm);
        //tanggal lahir Mahasiswa berada pada digit 4-11 npm
        String tanggalLahir = npmString.substring(4, 12);
        //mengubah String tanggalLahir menjadi integer
        int tanggal = Integer.parseInt(tanggalLahir.substring(0,2));
        int bulan = Integer.parseInt(tanggalLahir.substring(2,4));
        int tahun = Integer.parseInt(tanggalLahir.substring(4));
        return (tanggal + "-" + bulan + "-" + tahun);
    }

    /* method untuk mengekstrak tanggal lahir Mahasiswa dari npm */
    public String extractJurusan(long npm) {
        String npmString = Long.toString(npm);
        //kodeJurusan Mahasiswa berada pada digit 2 dan digit 3 npm
        String kodeJurusan = npmString.substring(2, 4);
        String jurusan = null;
        //jika kodejurusan 01, Mahasiswa Ilmu Komputer
        if (kodeJurusan.equals("01")) {
            jurusan = "Ilmu Komputer";
        //jika kodejurusan 02, Mahasiswa Sistem Informasi
        } else if (kodeJurusan.equals("02")) {
            jurusan = "Sistem Informasi";
        }
        return jurusan;
    }
    
    /* method tambahan */ 
    
    /* method untuk cek apakah mataKuliah sudah pernah didaftarkan */
    public boolean sudahTerdaftar(MataKuliah mataKuliah) {
        boolean terdaftar = false;
        //cek apakah terdapat objek MataKuliah tersebut dalam array daftarMataKuliah
        for (int i = 0 ; i < this.daftarMataKuliah.length ; i++){
            if (this.daftarMataKuliah[i] != null){
                if(this.daftarMataKuliah[i].equals(mataKuliah)){
                    terdaftar = true; //jika ditemukan objek MataKuliah tersebut, return true
                    break;
                }
            }
        }
        return terdaftar; 
    }

    /* method untuk cek apakah Mahasiswa memiliki Matkul */
    public boolean mataKuliahKosong() {
        boolean kosong = true;
        for (int i = 0 ; i < this.daftarMataKuliah.length ; i++){
            if (this.daftarMataKuliah[i] != null){ //cek apakah terdapat objek mahasiswa di dalam array daftarMataKuliah
                kosong = false;
                break; 
            }
        }   
        return kosong;
    }

    /* getter */

    public long getNpm() {
        return npm;
    }

    public String getJurusan() {
        return jurusan;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public MataKuliah[] getDaftarMataKuliah() {
        return daftarMataKuliah;
    }

}