package assignments.assignment3;
import java.util.*;

public class Main {
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalMataKuliah = 0;
    private static int totalElemenFasilkom = 0; 

    /* method untuk membuat objek Mahasiswa dan menambahkan objek tersebut ke dalam daftarElemenFasilkom */
    public static void addMahasiswa(String nama, long npm) {
        Mahasiswa mahasiswa = new Mahasiswa(nama, npm);
        addElementToArray(mahasiswa);
        System.out.println(nama + " berhasil ditambahkan");
    }

    /* method untuk membuat objek Dosen dan menambahkan objek tersebut ke dalam daftarElemenFasilkom */
    public static void addDosen(String nama) {
        Dosen dosen = new Dosen(nama);
        addElementToArray(dosen);
        System.out.println(nama + " berhasil ditambahkan");
    }

    /* method untuk membuat objek ElemenKantin dan menambahkan objek tersebut ke dalam daftarElemenFasilkom */
    public static void addElemenKantin(String nama) {
        ElemenKantin elemenKantin = new ElemenKantin(nama);
        addElementToArray(elemenKantin);
        System.out.println(nama + " berhasil ditambahkan");
    }

    /* method untuk melakukan aktivitas menyapa */
    public static void menyapa(String objek1, String objek2) {
        ElemenFasilkom elemen1 = getElement(objek1);
        ElemenFasilkom elemen2 = getElement(objek2);
        if (elemen1.getNama().equals(elemen2.getNama())) {
            //objek yang sama tidak melakukan method menyapa
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } else {
            elemen1.menyapa(elemen2);
        }
    }

    /* method untuk menambahkan Makanan yang dijual ElemenKantin */
    public static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom elemen = getElement(objek);
        //objek harus merupakan ElemenKantin
        if (elemen.getTipe().equals("ElemenKantin")) {
            ElemenKantin penjual = (ElemenKantin)getElement(objek);
            penjual.setMakanan(namaMakanan, harga); //implementasi method setMakanan objek ElemenKantin
        } else {
            System.out.println("[DITOLAK] " + elemen + " bukan merupakan elemen kantin");
        }
    }

    /* method untuk melakukan aktivitas membeli Makanan yang dijual ElemenKantin */
    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom elemen1 = getElement(objek1);
        ElemenFasilkom elemen2 = getElement(objek2);
        //objek harus merupakan ElemenKantin
        if (elemen2.getTipe().equals("ElemenKantin")) {
            //objek ElemenKantin tidak dapat membeliMakanan objek kantin itu sendiri
            if (elemen1.getNama().equals(elemen2.getNama())) {
                System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            } else {
                elemen1.membeliMakanan(elemen1, elemen2, namaMakanan); //implementasi method membeliMakanan objek ElemenFasilkom
            }
        } else {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
    }

    /* method untuk membuat objek MataKuliah dan menambahkan objek tersebut ke dalam daftarMataKuliah */
    public static void createMatkul(String nama, int kapasitas) {
        MataKuliah mataKuliah = new MataKuliah(nama, kapasitas);
        for (int i = 0 ; i < daftarMataKuliah.length ; i++){
            // mengganti objek null yang ditemukan pertama kali dengan objek MataKuliah
            if (daftarMataKuliah[i] == null){ 
                daftarMataKuliah[i] = mataKuliah;
                break; 
            }
        }
        totalMataKuliah++; //increment totalMataKuliah
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    /* method untuk menambahkan Matkul yang dimiliki Mahasiswa */
    public static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElement(objek);
        //objek harus merupakan elemen Mahasiswa
        if (elemen.getTipe().equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa)elemen;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            mahasiswa.addMatkul(mataKuliah); //mengimplementasikan method addMatkul objek Mahasiswa
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }
    }

    /* method untuk menghilangkan suatu Matkul yang dimiliki Mahasiswa */
    public static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElement(objek);
        //objek harus merupakan elemen Mahasiswa
        if (elemen.getTipe().equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa)elemen;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            mahasiswa.dropMatkul(mataKuliah); //mengimplementasikan method dropMatkul objek Mahasiswa
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    /* method menambahkan Matkul yang diajar Dosen */
    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElement(objek);
        //objek harus merupakan elemen Dosen
        if (elemen.getTipe().equals("Dosen")) {
            Dosen dosen = (Dosen)elemen;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            dosen.mengajarMataKuliah(mataKuliah); //mengimplementasikan mengajarMataKuliah objek Dosen
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }

    /* method berhenti mengajar Matkul yang diajar Dosen */
    public static void berhentiMengajar(String objek) {
        ElemenFasilkom elemen = getElement(objek);
        //objek harus merupakan elemen Dosen
        if (elemen.getTipe().equals("Dosen")) {
            Dosen dosen = (Dosen)elemen;
            if (dosen.getMataKuliah() != null) {
                System.out.println(objek + " berhenti mengajar " + dosen.getMataKuliah());
            }
            dosen.dropMataKuliah(); //mengimplementasikan dropMataKuliah objek Dosen
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    /* method untuk menampilkan ringkasan Mahasiswa */
    public static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom elemen = getElement(objek);
        //objek harus merupakan elemen Mahasiswa
        if (elemen.getTipe().equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa)elemen;
            //menampilkan nama, tanggal lahir, jurusan, dan daftar mata kuliah yang sedang diambil.
            String res = "Nama: " + mahasiswa.getNama() + '\n';
            res += "Tanggal Lahir: " + mahasiswa.extractTanggalLahir(mahasiswa.getNpm()) + '\n';
            res += "Jurusan: " + mahasiswa.extractJurusan(mahasiswa.getNpm()) + '\n';
            res += "Daftar Mata Kuliah: ";
            System.out.println(res);

            //cek apakah ada mahasiswa mengambil suatu mataKuliah
            if (mahasiswa.mataKuliahKosong()) { //jika belum mengambil mataKuliah
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                for (int i = 0; i < mahasiswa.getDaftarMataKuliah().length; i++){
                    if (mahasiswa.getDaftarMataKuliah()[i] != null) {
                        System.out.println((i + 1) + ". " + mahasiswa.getDaftarMataKuliah()[i].getNama()); //cetak nama dari tiap objek MataKuliah
                    }
                }
            }
        } else {
            System.out.println("[DITOLAK] " + objek + " bukan merupakan seorang mahasiswa");
        }
    }

    /* method untuk menampilkan ringkasan MataKuliah */
    public static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        //menampilkan nama mata kuliah, jumlah mahasiswa, kapasitas, dosen pengajar, dan daftar mahasiswa yang mengambil mata kuliah tersebut
        String res = "Nama mata kuliah: " + mataKuliah.getNama() + '\n';
        res += "Jumlah mahasiswa: " + mataKuliah.getTotalMahasiswa() + '\n';
        res += "Kapasitas: " + mataKuliah.getKapasitas() + '\n';

        //cek apakah mataKuliah memiliki Dosen
        if (mataKuliah.getDosen() == null) {
            res += "Dosen pengajar: Belum ada" + '\n';
        } else {
            res += "Dosen pengajar: " + mataKuliah.getDosen().getNama() + '\n';
        }
        res += "Daftar mahasiswa yang mengambil mata kuliah ini: ";
        System.out.println(res);

        //cek apakah mataKuliah mempunyai Mahasiswa
        if (mataKuliah.getTotalMahasiswa() == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            for (int i = 0; i < mataKuliah.getDaftarMahasiswa().length; i++){
                if (mataKuliah.getDaftarMahasiswa()[i] != null) {
                    System.out.println((i + 1) + ". " + mataKuliah.getDaftarMahasiswa()[i].getNama()); //cetak nama dari tiap objek MahaSiswa
                }
            } 
        }
    }

    /* method untuk mengakhiri hari */
    public static void nextDay() {
        for (ElemenFasilkom elemen : daftarElemenFasilkom) {
            if (elemen != null) {
                //perhitungan atribut friendship
                //jika menyapa >= setengah total elemen, mendapat tambahan 10 nilai
                if (elemen.totalMenyapa() >= (totalElemenFasilkom)/2) {
                    elemen.setFriendship(elemen.getFriendship()+10);
                } else {
                    //jika menyapa < setengah total elemen, mendapat pengurangan 5 nilai
                    elemen.setFriendship(elemen.getFriendship()-5);
                }
                //cek nilai atribut friendship agar tidak melewati interval
                elemen.cekFriendship();
            }
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate.");

        //mengurutkan dan mencetak atribut friendship dari tiap objek ElemenFasilkom
        friendshipRanking();

        //menghapus semua objek ElemenFasilkom dari atribut array telahMenyapa miliki tiap objek ElemenFasilkom
        for (ElemenFasilkom elemen : daftarElemenFasilkom) {
            if (elemen != null) {
                elemen.resetMenyapa();
            }
        }

    }

    /* method untuk mengurutkan dan mencetak atribut friendship dari tiap objek ElemenFasilkom */
    public static void friendshipRanking() {
        //mengurutkan ElemenFasilkom dari nilai atribut friendship tertinggi (prioritas) dan alphabetic
        Arrays.sort(daftarElemenFasilkom, new ElemenSortingComparator());

        // mencetak atribut friendship dari ElemenFasilkom dari array daftarElemenFasilkom yang telah diurutkan
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] != null) {
                System.out.println((i+1) + String.format(". %s(%d)", daftarElemenFasilkom[i].getNama(), daftarElemenFasilkom[i].getFriendship()));
            }  
        }
    }

    /* method untuk mengakhiri program */
    public static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom: ");
        //mengurutkan dan mencetak atribut friendship dari tiap objek ElemenFasilkom
        friendshipRanking();
    }

    /* method tambahan */

    /* method untuk memasukkan ElemenFasilkom ke array daftarElemenFasilkom */
    public static void addElementToArray(ElemenFasilkom elemenFasilkom) {
        for (int i = 0 ; i < daftarElemenFasilkom.length ; i++){
            if (daftarElemenFasilkom[i] == null){ 
                daftarElemenFasilkom[i] = elemenFasilkom; // mengganti objek null yang ditemukan pertama kali dengan objek ElemenFasilkom
                break; 
            }
        }
        totalElemenFasilkom++; //increment nilai atribut totalElemenFasilkom
    }

    /* method untuk mengambil objek ElemenFasilkom pada daftarElemenFasilkom */
    public static ElemenFasilkom getElement(String elemenFasilkom) {
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(elemenFasilkom)) {
                return daftarElemenFasilkom[i]; //return objek ElemenFasilkom yang memiliki atribut Nama elemenFasilkom
            }
        }             
        return null;
    }

    /* method untuk mengambil objek MataKuliah pada daftarMataKuliah */
    public static MataKuliah getMataKuliah(String mataKuliah) {
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i].getNama().equals(mataKuliah)) {
                return daftarMataKuliah[i]; //return objek MataKuliah yang memiliki atribut Nama mataKuliah
            }
        }             
        return null;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}