package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    private Makanan[] daftarMakanan = new Makanan[10];

    /* Konstruktor */
    ElemenKantin(String nama) {
        this.setNama(nama);
        this.setTipe("ElemenKantin");
    }

    /* getter */
    public Makanan[] getDaftarMakanan() {
        return daftarMakanan;
    }

    /* method untuk menambahkan Makanan yang dimiliki ElemenKantin */
    public void setMakanan(String nama, long harga) {
        Makanan makanan = new Makanan(nama,harga); //membuat objek Makanan
        //jika ElemenKantin sudah memiliki objek Makanan tersebut dalam daftarMakanan
        //tidak dapat melakukan method setMakanan
        if (sudahTerdaftar(makanan)) {
            System.out.println("[DITOLAK] " + makanan.getNama() + " sudah pernah terdaftar");
        } else {
            //memasukkan objek Makanan ke array daftarMakanan
            for (int i = 0 ; i < this.daftarMakanan.length ; i++) {
                // mengganti objek null yang ditemukan pertama kali dengan objek Makanan
                if (this.daftarMakanan[i] == null) {
                    this.daftarMakanan[i] = makanan;
                    System.out.println(this.getNama() + " telah mendaftarkan makanan " + makanan.getNama() + " dengan harga " + makanan.getHarga());
                    break;
                }
            }
        }
    }

    /* method tambahan */
    
    /* method untuk cek apakah objek Makanan sudah pernah didaftarkan */
    public boolean sudahTerdaftar(Makanan makanan) {
        boolean terdaftar = false;
        for (int i = 0 ; i < this.daftarMakanan.length ; i++){
            //cek apakah terdapat objek Makanan tersebut dalam array daftarMakanan
            if (this.daftarMakanan[i] != null){
                if(this.daftarMakanan[i].equals(makanan)){
                    terdaftar = true; //jika ditemukan objek Makanan tersebut, return true
                    break;
                }
            }
        }
        return terdaftar; 
    }

    /* method untuk mengambil objek makanan pada daftarMakanan */
    public Makanan getMakanan(String namaMakanan) {
        for (int i = 0; i < this.getDaftarMakanan().length; i++) {
            if (this.getDaftarMakanan()[i] != null ) {
                if (this.getDaftarMakanan()[i].getNama().equals(namaMakanan)) {
                    return this.getDaftarMakanan()[i]; //return objek Makanan yang memiliki atribut Nama namaMakanan
                } 
            }  
        }             
        return null;
    }
}